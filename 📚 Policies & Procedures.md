# 📚 Policies & Procedures

This section of the Employee Handbook details our main policies and procedures. Please read them carefully and ensure that you understand what is required. You can always refer to this Handbook to remind yourself at any time of what to do.


These policies and procedures may change from time to time or may be updated, therefore please ensure that you familiarise yourself with the most recent policy / procedure.


If you are looking for a policy which doesn’t appear to be listed, contact [HIDDEN] to request more details.