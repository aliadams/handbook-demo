**!! NOTE** this is a frozen version of the Telcom Handbook for non team viewers. The live version is available in the team KnowledgeBase.



# 🚀 Welcome to Telcom

# 👋 Hi!

Welcome to Telcom. We hope you will enjoy working with us to deliver quality services to all of our clients. We expect a lot from you, but you can expect a lot from us too!


This Handbook will help guide and inform you throughout your employment.


> I would like take this opportunity to welcome you to Telcom and look forward to working with you to achieve the ambitious goals set for this company.
>
> \
> To do this, we will require commitment from you and we will do our best to support you through a comprehensive induction process and opportunities to undertake appropriate training to develop your knowledge and enhance your skills.
>
> \
> This Handbook is part of this commitment and will help you to learn about this company and how we work.
>
> \
> Finally, I would like to wish you every success and good luck in your new job.


**Shaun Gibson**

Telcom Group CEO





---

# **Let’s get started**

Below are the main bits of information you should know before you get started.

## Contract of Employment/Company Rules

Your Contract of Employment describes the nature of your relationship with the Company. Please ensure that you read your Contract of Employment carefully and if necessary raise any points with your Manager for clarification. It is your responsibility to ensure that you are fully aware of all aspects of your employment. Telcom reserves the right to make reasonable changes to any of your terms and conditions of employment including reviewing your salary. Any changes made to your terms and conditions will be put in writing to you at the earliest opportunity. In any organisation it is necessary to have certain rules and regulations to protect the health and safety of all employees and customers and to ensure high standards of conduct, performance and service. As an employee of Telcom, it is important that you are aware of the rules and regulations which apply to you. Failure to adhere to Company rules, procedures and policies will result in disciplinary action, which may include dismissal, in accordance with the Company’s discipline procedure.

## Compliance

Every employee must, at all times act within the law of the land and any regulations which are applicable to the Company’s activities. Any employee, who becomes aware of another employee acting illegally, whilst acting on behalf of the Company, must report the activity to a Manager.

## Systems and Procedures

All administrative and operational systems and procedures must be strictly adhered to. Employees are required to report any irregularities in systems, procedures or documentation to a Manager.

## The Induction Programme

We currently operate a two tier induction programme which is designed to help you to settle into your new role and later to help you gain a better understanding of aims and objectives and how we aim to fulfil them.
| Stage | What happens |
|----|----|
| Initial Induction | This will take place at your base location and will cover all domestic arrangements including Facilities, Fire safety, Health and Safety, Hours of Work and Lunch Breaks etc. \n  \n The initial induction will normally be carried out by your Manager or a nominated trained representative and will be completed over the first two weeks of employment. \n  \n Remember to feel free to ask questions throughout this time. |
| Department Induction | Aim: To give new employees an understanding of what we do, how we do it and to understand the importance of providing a professional and effective quality service to all our clients. \n  \n Objectives: \n  \n • To recognise the vision for the future and the core values of the company. \n • To be aware of the services provided. \n • To understand the required standards in your role and set your individual objectives for next 6 months |

## Equality, Diversity and Inclusion

Telcom is committed to promoting equality of opportunity, tackling discrimination and valuing the diversity of both our staff and the communities we serve. Telcom wishes to create and sustain an organisational culture which values people from all sections of the community and the contribution each individual can make to Telcom work. Telcom is committed to promoting equality throughout the organisation and requires all staff, and partners to participate fully in achieving our aims. Telcom will ensure appropriate training, guidance and advice are provided to meet these aims and commitment.


The equal opportunities policy statement and policy statement on harassment at work are designed to implement the commitment of Telcom to equal opportunities. It is the responsibility of every employee to ensure his or her own conduct conforms to the expected standards and reflects these policy statements.


The aim of the policy is to encourage harmony and respect amongst individuals so as to promote good working practices with a view to maximising the performance and the return to Telcom and the employees.

## Health and Safety

The Company has adopted a policy to ensure the health, safety and welfare of all employees. Every employee must ensure that his/her conduct conforms to the standards set out in the [Health and Safety policy](/doc/49-health-and-safety-policy-t1PSOIcKLX).

## Job Title

Your role within Telcom will have been outlined to you during your interview and your contract of employment. As our business is constantly changing, your job title may also change to meet business needs. If you have any questions about the content of your job, please ask your immediate Manager. Your role may require flexibility and the Company reserves the right after prior consultation to make adjustments and changes to your role.

## Job Location

Most employees work from one site. However, some employees may be required to work at different locations from time to time or even work field / patch based. If due to business and economic reasons this becomes necessary, you will be consulted.

## Probationary Period

Your employment is subject to a probationary period and will be reviewed at 3 months. During this period, Telcom will assess and review your job performance. This probationary period may be extended at Telcom discretion up to a maximum of 12 months. You will receive written confirmation that you have successfully completed your probationary period.

During your probationary period, the notice period on either side will be one week’s notice.

## Continuous Service

Your continuous service commences from the date when you first joined Telcom (providing you have not left and subsequently re-joined) or, if you are a TUPE transferee to Telcom, the date on which you commenced work for your previous employer.

## Pay

Your pay frequency will be either weekly or monthly and will be clearly indicated within your Contract of Employment.


If at any time, you believe that your pay has been calculated in error or if you do not understand how your pay has been calculated, please do not hesitate to contact your Manager. Errors will be rectified as soon as possible.

## Income Tax and National Insurance

At the end of each tax year you will be given a form P60 showing the total pay you have received from us during that year and the amount of deductions for Income Tax and National Insurance. You may also be given a form P11D showing non-salary benefits. You should keep these documents in a safe place as you may need to produce them for tax purposes. When you first start with Telcom, you should provide your line manager with your P45 from your previous organisation to ensure you are being taxed at the correct rate. If you are unable to provide a P45 you will be given a HRMC Starter Checklist to complete. If you have any questions about your income tax, please get in touch with the tax office. You’ll be asked for your surname, National Insurance number and the company PAYE.

## Change of Details

Should any of your details have been incorrectly recorded, or any personal details change within your employment then please let us know so that we can always ensure our records are up to date. You can read more about how to do keep us updated on the [If your personal details change](/doc/if-your-personal-details-change-S8ncVhtfnG) page.


