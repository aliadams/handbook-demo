# 😇 Code of Conduct

At Telcom our goal is to give every one of our employees a safe, reliable and happy place to work. The Telcom Code of Conduct is a list of the main guidelines we expect our team to adhere to in order to achieve that.


If you’d like to read our more detailed policies on particular areas, check our our [Policies & Procedures section](/doc/policies-and-procedures-2HocEJnIIH).



---

# Acting appropriately at work

We must all ensure that the highest standards of behaviour prevail. Telcom expects each employee to maintain the highest possible standard of professional conduct and behaviour.


At all times you must comply with the rules and procedures of any association or professional body to which Telcom and/or you may from time to time belong to.


You must notify Telcom of any criminal convictions incurred or awarded during the course of your employment.


Employees are not permitted to have personal friends, including family members on premises of Telcom, unless they are legitimate customers of the business or are performing a service as authorised by a Director.


Information relating to any individual employee’s private affairs shall not be supplied to any person or organisation external to Telcom unless the employee has consented to the release of the information or there is a legal obligation on Telcom to do so.


You are responsible for safeguarding your personal property whilst on Company premises. The Company accepts no liability for loss or damage to personal property regardless of the circumstances.

# **Maintain appropriate appearance and dress**

Telcom recognises that all employees are individuals. All employees must wear appropriate clothing and footwear. Appropriate in this instance means in keeping with the image of the Company


For further information, see the [Dress Code Policy](/doc/411-dress-code-policy-thCncSAtKS).

# **Drive safely**

Telcom is committed to the delivery of its obligations under the Health and Safety legislation with regard to driving at work, and expects employees to comply with all relevant legislation in relation to driving.


The Company discourages the use of mobile phones whilst driving even if a hands free is fitted.


For further information, see the [Company Vehicle Policy](/doc/412-company-vehicle-policy-h78h6959T5).


# Follow company systems and procedures when they are in place

All administrative and operational systems and procedures must be strictly adhered to. Employees are required to report any irregularities in systems, procedures or documentation to a Manager.

# **Respect confidentiality and intellectual property**

You must not during or after your employment, use for your own purposes, divulge to any third party or cause any unauthorised disclosure of any trade secrets or confidential information relating to the Company and/or any Associated Company and clients.


All notes, memoranda, records, lists of customers and suppliers and employees, correspondence, documents, computer hardware and software and other disc and tapes, data listings, codes, designs and drawings and other documents and material whatsoever (whether made or created by you or otherwise) relating to the business of the Company or any Associated Company (and any copies of the same):


* shall be and remain the property of the Company or the relevant Associated Company; and
* shall be handed over by you to the Company or to the relevant Associated Company on demand and in any event on the termination of the Employment.

## **Don’t copy company documents or other intellectual property**

In the course of our work you will come across many documents produced either by the Company or the Customer. It is against Company rules to make copies of any documents for any other purpose than as required by the Company or the Customer. You must return all documents if requested to do so.

## Inventions arising from employment are considered intellectual property of the company

Any idea, invention, design or improved production method in related systems arising from your employment or duties specifically assigned to you shall, as far as you have control over them, be the property of the Company. This applies whether it is achieved by you or jointly with another person(s).

Where appropriate, the Company will register any invention or design and you are required to give every assistance, even where you are no longer in the Company’s employment. The copyright of any material produced by you in the course of your employment or related to your employment rests with the Company.

# **Personal Correspondence and Telephone Calls**

Personal Mobile phones are not to be used by employees during working hours. Mobile phones must be placed on discreet or silent at all times. Calls may only be made or accepted for emergency reasons with permission from your Manager.


Employees are not permitted to use Telcom stationary or postage for personal correspondence.

# **Use company IT equipment for business use only**

The Computer E-mail/Internet Usage Policy and Monitoring Policy, is designed to protect the Company’s computer equipment, prevent inappropriate use and protect confidential data stored on computer files. Every employee must ensure that his/her conduct conforms to the standards set out in this policy. Failure to comply may result in disciplinary action and, in serious cases, dismissal.

# **Be careful about representing the company publicly**

Every employee is responsible for promoting the reputation and image of the Company. Employees must not make detrimental statements in respect of the Company during the course of dealings with individuals outside of the Company. No employee is permitted to give press or other media interviews or assist with or be involved in the publication of any article relating to the business affairs of the Company or in relation to the Company’s intellectual property.


The organisation recognises that many employees make use of social media in a personal capacity. While they are not acting on behalf of the organisation, employees must be aware that they can damage the organisation if they are recognised as being one of our employees.


Employees are allowed to say that they work for the organisation, which recognises that it is natural for its staff sometimes to want to discuss their work on social media. However, the employee's online profile for example, the name of a blog or a Twitter name, must not contain the organisation's name.

If employees do discuss their work on social media for example, giving opinions on their specialism or the sector in which the organisation operates, they must include on their profile a statement along the following lines: "The views I express here are mine alone and do not necessarily reflect the views of my employer."


For further information, see the Social Media Policy.

# **Don’t smoke on the premises or in company vehicles**

In the interests of the health, safety and comfort of employees and customers and to comply with legislation, the Company operates a no smoking policy. Smoking is prohibited in all Company work places and Company owned vehicles, except in designated smoking areas.

For further information, see the No Smoking Policy.

# **Don’t consume or distribute alcohol or drugs**

The Company forbids the consumption of alcohol on its premises nor will it permit any employee to work whilst under the influence of alcohol and/or drugs.


The Company forbids the possession, use or distribution of drugs for non-medical purposes on its premises.


For further information, see the Alcohol and Drugs Policy.

# **Take care giving or receiving gifts or hospitality**

The giving and receiving of gifts, hospitality or services is generally not permitted, as it is easy to be accused of accepting bribes. Exceptions include groups of colleagues clubbing together to buy another colleague a wedding present for example.


Please speak to your Manager for further details

# **You can document and claim expenses using the Expense Claims procedure**

Company policy is that, subject to HM Revenue & Customs rules and regulations, reasonable expenses, wholly, necessarily and exclusively incurred on company business will be promptly repaid. All employees must ensure that expenses incurred and charged to the company are in the interest of the company and that the cost is kept to a minimum.


The company reserves the right not to repay expenses when the claim has not been made within 3 months of the date when the cost was incurred or in line with the [Expenses Claims Procedure](/doc/expenses-policy-how-to-claim-Y1j8HTDOJS). Fraud or abuse of Business Expenses will be treated seriously and may lead to dismissal.

# **Get written permission before volunteering or working elsewhere during your employment**

Your employment with the Company must be viewed as your primary employment. Written permission must be obtained before accepting any other form of employment/volunteering (Including self-employment). Permission will not be unreasonably refused but will only be granted if:


* there is no potential conflict of interest;
* the demands of the secondary employment/volunteering are not such that they may adversely affect in any way your capability to fulfil the demands of your primary employment with the Company.


Before accepting secondary employment/volunteering, you must discuss the matter with your Manager, who may take advice from a Director.


Secondary employment/volunteering must not be conducted on Company premises or during normal working hours.


Anyone undertaking or accepting secondary employment/volunteering without obtaining written permission from the Company will be dealt with in accordance with the Company’s disciplinary procedure.

# **Work with ethics and integrity**

We expect each of our employees to maintain the highest standards in carrying out their business activities, adhering to legislation and our policies on business conduct. We expect you always to act professionally, honestly and ethically in your dealings with colleagues, third parties and customers.

If during your employment you have received any convictions that you believe will impact on our reputation or the job that you do, it is important that you notify your manager of this fact as soon as possible as it may have a bearing on your future employment with us.


For the same reasons outlined above you should also advise your manager if you are required to attend court as a result of being arrested or charged with a criminal or statutory offence; or have any economic or terrorism related sanctions against you, as this may also have a bearing on your future employment with Telcom.

# **Ensure you are eligible to work**

Telcom has a legal obligation only to employ individuals who are eligible to work in the United Kingdom (UK). Telcom conducts checks of relevant documentation to ensure that it only offers employment to individuals who are eligible to work in the UK. Should your immigration status or ability to work legally in the UK change, you are obliged, as a condition of your continued employment, to keep the Company up-to-date with this information and to provide acceptable and specified evidence of your right to work in the UK. Where there is a restriction on the length of time you are eligible to stay or work in the UK, there are additional checks which Telcom will need to make around the time that your entitlement to live and work in the UK expires. Telcom may need to provide information relating to you and your employment to the UK Border Agency. By accepting and being in employment with Telcom you consent to the disclosure of this information to the UK Border Agency. Any employee who is unable to prove their eligibility to work in the UK may, following a reasonable investigation, may be dismissed on these grounds.

# **Respect disciplinary procedure**

Telcom’s aim is to encourage improvement in individual conduct. The disciplinary procedure is designed to help and encourage all employees to achieve and maintain good standards of conduct, attendance and job performance. The disciplinary procedure sets out the action which will be taken when disciplinary rules are breached. The procedure is designed to establish the facts quickly and to deal consistently with disciplinary issues. No disciplinary action will be taken until the matter has been fully investigated.


For further information, see the [Disciplinary Policy](/doc/47-disciplinary-and-dismissal-policy-and-procedure-XQoCUB9BU8).

# **Let use know about any issues or grievances you have**

Telcom recognises that from time to time employees may wish to seek redress for grievances relating to their employment. In this respect, the grievance policy is to encourage free communication between employees and their managers to ensure that questions and problems arising during the course of employment can be aired and, where possible, resolved quickly and to the satisfaction of all concerned.


For further information, see the [Grievance Policy](/doc/48-grievance-policy-and-procedure-4imBiE4rpp)

# **Expand your skills with our learning and development policy**

The Company recognises the utmost importance of learning and development in order to ensure that all our employees have the high level of knowledge, skills and overall competence to exceed customer expectations and to achieve the Company goals.


You will be required to participate in the Appraisal Scheme and attend further training and development approved by Telcom to ensure your skills are current.


Telcom has paid for training or development opportunities and the employee subsequently resigns from Telcom, you may be liable to repay some or all of the costs of that training. For further information, see the [Training Repayment Policy](/doc/training-repayment-policy-pL6kcqR97A).

# **Disclosures and Disclosure Information**

Telcom uses the Disclosure and Barring Service (DBS) service to help assess the suitability of applicants for positions of trust, we are recipients of Disclosure information and must comply fully with the DBS Code of Practice.


Amongst other things, this obliges the Company to have written policy on the correct handling and safekeeping of Disclosure information.


To help assess the suitability of applicants for positions of trust, the Company complies fully with the DBS Code of Practice regarding the correct handling, use, storage, retention and disposal of Disclosures and Disclosure information.


It also complies fully with its obligations under the Data Protection Act and other relevant legislation pertaining to the safe handling, use, storage, retention and disposal of Disclosure information and has a written policy on these matters, which is available to those who wish to see it on request.

Disclosure information is never kept on an applicant’s personnel file and is always kept separately and securely, in lockable, non-portable storage containers with access strictly controlled and limited to those who are entitled to see it as part of their duties.


In accordance with Section 124 of the Police Act 1997, Disclosure information is only passed to those who are authorised to receive it in the course of their duties. We maintain a record of all those to whom Disclosures or Disclosure information has been revealed and we recognise that it is a criminal offence to pass this information to anyone who is not entitled to receive it.


Disclosure information is only used for the specific purpose for which it was requested and for which the applicant’s full consent has been given.


Once the recruitment or other relevant decision has been made, the Disclosure information is not held for any longer than is absolutely necessary. This is generally for a period of up to six months to allow for the consideration and resolution of any disputes or complaints. If, in view of very exceptional circumstances, it is considered necessary to keep Disclosure information for longer than six months the Company will consult the DBS about this and give full consideration to the Data Protection and Human Rights Act before doing so. During this period the usual conditions regarding safe storage and strictly controlled access will prevail.


Once the retention period has elapsed, any Disclosure information will immediately be suitably destroyed by secure means.


Photocopies or other image of the Disclosure will not be retained. However, notwithstanding the above, the Company may keep a record of any or all of the following;


* The date of issue of a Disclosure;
* The name of the subject;
* The type of Disclosure requested;
* The position for which the Disclosure was requested;
* The unique reference number of the Disclosure;
* The details of the recruitment decision.


