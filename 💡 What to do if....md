# 💡 What to do if...

Sometimes stuff happens. Like getting ill or the building going on fire.

Here are some quick guides on what to do in those cases: