# 4.22 Environmental Policy

Telcom Networks and Telcom Infrastructure are committed to providing a quality service in a manner that ensures a safe and healthy workplace for our employees and minimises our potential impact on the environment. We will operate in compliance with all relevant environmental legislation and we will strive to use pollution prevention and environmental best practices in all we do.

## We will:

* integrate the consideration of environmental concerns and impacts into all of our decision making and activities,
* promote environmental awareness among our employees and encourage them to work in an environmentally responsible manner,
* train, educate and inform our employees about environmental issues that may affect their work,
* reduce waste through re-use and recycling and by purchasing recycled, recyclable or re-furbished products and materials where these alternatives are available, economical and suitable,
* promote efficient use of materials and resources throughout our facility including water, electricity, raw materials and other resources, particularly those that are non-renewable,
* avoid unnecessary use of hazardous materials and products, seek substitutions when feasible, and take all reasonable steps to protect human health and the environment when such materials must be used, stored and disposed of,
* purchase and use environmentally responsible products accordingly,
* where required by legislation or where significant health, safety or environmental hazards exist, develop and maintain appropriate emergency and spill response programmes,
* communicate our environmental commitment to clients, customers and the public and encourage them to support it,
* strive to continually improve our environmental performance and minimise the social impact and damage of activities by periodically reviewing our environmental policy in light of our current and planned future activities.

 \n 