# Company Material & Property

In the course of your employment, you will have access to and be entrusted with information in respect of the business of Telcom Networks and any Group Company and its or their dealings, transactions and affairs, all of which information is or may be confidential. You shall not (except in the proper course of your duties) during or after your employment divulge to any person, firm or corporation whatsoever or otherwise make use of (and shall use your best endeavours to prevent the publication or disclosure of) any trade secret and/or any confidential information concerning the business of Telcom Networks and Group Companies, its and their dealings, transactions or affairs, or any such confidential information concerning its or their products, suppliers, customers, agents  or distributors. You shall not seek to exploit in any way, or otherwise, make use of any confidential information. This provision shall survive the termination of your employment.


"Confidential information" includes but is not limited to the following:


* trade secrets;
* details of the business and finances of Telcom Networks and its Group Companies;
* the contents of and set-up of the IT systems operated by Telcom Networks;
* any databases of clients and suppliers;
* other information generally relating to Telcom Networks or any Group Company (whether or not such information is recorded in writing or on computer disk or tape);
* details of other employees, directors and consultants of Telcom Networks or any Group Company, including but not limited to details of their salary and health;
* the prices at which goods and services are supplied to Telcom Networks or any Group Company by suppliers;
* details of any negotiations taking place with existing or potential suppliers and existing or potential clients of Telcom Networks or any Group Company;
* details of Telcom Network’s or any Group Company's business plans, profits, expenses and financial arrangements with third parties;
* information as to the identity, requirements, or finances of third parties; and
* any other information which is notified to you as being confidential or is marked as such whether relating to Telcom Networks or any Group Company or third party or any employee of Telcom Networks, any Group Company or any third party.

The restrictions contained in this section of the company policies will cease to apply to information or knowledge which may become public and readily available except as a result of any act or omission by you.

You should not remove from Telcom Network’s premises (save where expressly required to do so in the course of your employment or with the prior express permission of Telcom Networks) any documents or other tangible items containing, or which might contain, confidential information.

You acknowledge and agree that:

* any record (whether printed, in writing or electronic) of any trade secret or confidential information will be and remain (as between Telcom Networks and you) the property of Telcom Networks or the relevant Group Company; and
* you will surrender any such record in your possession custody or power to Telcom Networks, or delete it immediately, on demand by Telcom Networks or any Group Company.

Nothing in this policy precludes you from making a protected disclosure as the same is defined in section 43A Employment Rights Act 1996.


For the purposes of this policy, "Group Company" means any parent undertaking of Telcom Networks and any subsidiary undertaking of Telcom Networks or of any such parent undertaking (where "parent undertaking" and "subsidiary undertaking" have the meanings attributed to them under section 1162 Companies Act 2006) including for the avoidance of doubt Telcom Networks Inc.