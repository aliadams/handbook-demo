# Whistleblowing Policy

# **What is whistleblowing?**

Whistleblowing is the disclosure of information which relates to suspected wrongdoing or dangers at work. This may include:

* criminal activity;
* miscarriages of justice;
* danger to health and safety;
* damage to the environment;
* failure to comply with any legal or professional obligation or regulatory requirements;
* bribery;
* financial fraud or mismanagement;
* breach of our internal policies and procedures including our [Code of Conduct](https://kb.telcom.uk/books/handbook/chapter/%3F-how-to-behave);
* conduct likely to damage Telcom Network’s reputation; and/or
* the deliberate concealment of any of the above matters.

A whistleblower is a person who raises a genuine concern in good faith relating to any of the above. If you have any genuine concerns related to suspected wrongdoing or danger affecting any of Telcom Network’s activities you should report it under this policy.

This policy should not be used for complaints relating to your own personal circumstances, such as the way you have been treated at work. In those cases you should use the [Grievance Procedure](https://kb.telcom.uk/books/handbook/page/grievance-procedure) policy.


# **Protection for whistleblowers**

It is understandable that whistleblowers are sometimes worried about possible repercussions. We aim to encourage openness and will support employees who raise genuine concerns in good faith under this policy, even if they turn out to be mistaken.


Employees must not suffer any detrimental treatment as a result of raising a concern in good faith. Detrimental treatment includes dismissal, disciplinary action, threats or other unfavourable treatment connected with raising a concern. If you believe that you have suffered any such treatment, you should inform the Shaun Gibson, Founder, immediately.


If the matter is not remedied you should raise it formally using our [Grievance Procedure](https://kb.telcom.uk/books/handbook/page/grievance-procedure). Employees must not threaten or retaliate against whistleblowers in any way. Anyone involved in such conduct will be subject to disciplinary action.


If anyone has any concerns with regard to practices, conduct and the operations within Telcom Networks, they should raise those concerns, in the first instance, with their Line Manager.

However, where the matter is more serious, or you feel that your Line Manager has not addressed your concern, or you prefer not to raise it with them for any reason, you should follow the [Reporting Escalation Policy](/doc/reporting-escalation-policy-NyzFmpkoUp) to determine who next to contact.


Following a disclosure, a full investigation will be undertaken. You will be advised of the progress of the investigation and any decisions taken. The whistleblowing procedure will only offer protection to those who make such disclosures in good faith. Anyone making a disclosure outside the scope of these guidelines may be subject to disciplinary action. Employees must not threaten or retaliate against whistleblowers in any way. Anyone involved in such conduct will be subject to disciplinary action.