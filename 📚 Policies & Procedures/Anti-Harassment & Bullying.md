# Anti-Harassment & Bullying

> All members of staff have the right to a working environment free from all forms of unlawful discrimination or bullying. Telcom will not permit or condone any form of discrimination, harassment, bullying or victimisation.


We expect all staff members to conduct themselves in a manner which is courteous and considerate and displays respect for the needs, sensitivities and dignity of colleagues and clients. We expect such behaviour in work and work related social activities, both during and outside working hours and on and off our premises. You have a personal responsibility to behave in a manner which is not, nor is likely to be perceived as, offensive to others. Particular care should be taken when social activities involve the consumption of alcohol.


Telcom will take allegations of harassment seriously and address them promptly and confidentially where possible. If you feel that you have suffered from harassment, discrimination, bullying or victimisation, you are encouraged to raise the matter through Telcom's [Grievance Procedure](/doc/48-grievance-policy-and-procedure-4imBiE4rpp).


All members of staff should take the time to ensure they understand what types of behaviour are unacceptable under this policy and should always consider whether their words or conduct could be offensive, as even unintentional harassment or bullying is unacceptable.

Any breach of this policy will render the person responsible liable to disciplinary action, including, for serious or repeated behaviour, summary dismissal. In addition, where discrimination constitutes a criminal offence, the individual could be liable to prosecution.

# **What is harassment?**

Harassment is any unwanted physical, verbal or non-verbal conduct which has the purpose or effect of violating a person's dignity or creating an intimidating, hostile, degrading, humiliating or offensive environment for them. A single incident can amount to harassment.


It also includes treating someone less favourably because they have submitted or refused to submit to such behaviour in the past.


Unlawful harassment may be persistent or an isolated incident and may involve conduct of a sexual nature (sexual harassment), or it may be related to a **Protected Characteristic**. Harassment is unacceptable even if it does not fall within any of these categories.


A person may be harassed even if they were not the intended "target". For example, a person may be harassed by racist jokes unrelated to their own ethnicity, if the jokes create an offensive environment for him.


Remember the intention behind your conduct is irrelevant. It is important not to behave in such a way that another person may perceive as harassment.

# **What is bullying?**

Bullying is offensive, intimidating, malicious or insulting behaviour involving the misuse of power that can make a person feel vulnerable, upset, humiliated, undermined or threatened. Power does not always mean being in a position of authority, but can include both personal strength and coercion through fear or intimidation.

Bullying can take the form of physical, verbal and non-verbal conduct.

Legitimate, reasonable and constructive criticism of a worker's performance or behaviour, or reasonable instructions given to workers in the course of their employment or engagement, will not amount to bullying on their own.

# **Informal steps**

If you are being bullied or harassed, you should initially consider raising the problem informally with the person responsible if you feel able. You should explain clearly to them that their behaviour is not welcome or makes you uncomfortable. If this is too difficult or embarrassing, you should speak to your Line Manager or another senior member of staff, who can provide confidential advice and assistance in resolving the issue formally or informally.


If informal steps have not been successful or are not possible or appropriate, you should raise a formal complaint through Telcom's[ Grievance Procedure](/doc/48-grievance-policy-and-procedure-4imBiE4rpp). Also see the page '[How to report harassment and bullying'](/doc/what-to-do-when-you-feel-harassed-or-bullied-lzOCWXOQzC) section for further informations on the steps you can take.



---


# Appendices

## Identifying sexual harassment


:::info
**Quick guide:**  If you are **discussing anyone's appearance, attractiveness, or any form of sex act**… there is a good chance you aren't making Telcom a comfortable place for everyone to work.

:::

Sexual harassment is a form of sex discrimination. It takes place when someone is subjected to unwelcome and unwanted sexual behaviour or other conduct related to their gender. \n  \n Behaviour that constitutes sexual harassment includes:


* Unwelcome behaviour of a sexual nature, this may be either physical or verbal
* Inappropriate or suggestive remarks or verbal sexual advances
* Indecent comments, jokes or innuendos relating to a person’s looks or private life
* Unwanted physical contact such as hugging, kissing or inappropriate touching
* Requests for sexual favours
* The display or circulation of pornography or indecent images

  \

Often, this kind of behaviour may be brushed off by the harasser as ‘banter’ or harmless flirting. It is important to remember that the impact the behaviour is the most important factor, it is not so relevant whether the individual intended to cause offence, but rather that offence was caused by the conduct.


Sexual harassment can happen regardless of the individuals’ gender, gender identity, or gender expression and can, for example, occur between same-sex individuals as well as between opposite-sex individuals, and does not require that the harassing conduct be motivated by sexual desire.