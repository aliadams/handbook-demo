# Training Repayment Policy

Telcom is committed to achieving value for money, and to ensure that funds are utilised as effectively as possible. Because of this, and in recognition of the fact that learning and development budgets are strictly limited, there is the provision, in the circumstances outlined below, to require individuals to repay the cost of learning and development.


If you fail to satisfactorily attend and/or satisfactorily complete the course or leave Telcom for whatever reason (except redundancy) before the end of the training course/course of study/examination then you will be liable for 100% of all the training course/course of study/examination Telcom  shall determine its sole discretion acting responsibly, whether you have satisfactorily completed the course for the purpose of this agreement.

## Conditions of repayment

1. If you resign or leave Telcom for whatever reason (except redundancy), or decide not to attend/complete training course/course of study/examination fees


1.1 before attending the training course/course of study/examination and where Telcom has not yet incurred liability for the cost of the training, Telcom Paymaster Ltd. will not pay any fees or associated costs. If you decide to attend, you will bear all costs;


1.2 before attending the training course/course of study/examination and where Telcom has incurred liability for the cost of the training, you will be responsible for repaying 100% of the fees and any associated costs to Telcom;


1.3 and you are still attending the training course/ course of study/examination where Telcom has incurred liability for the cost of the training, you will be responsible for repaying 100% of the fees and any associated costs to Telcom ;


2. If you subsequently leave Telcom for whatever reason (except redundancy) for a minimum period of two years after the date of completion of training course/course of study/examination you will be liable to repay the cost of the training course/course of study/examination based on the sliding scale in table 1 below.


3. It is at the discretion of Telcom if you will be allowed paid time off to attend the training course/course of study/examination. If you resign and require time off to attend training course/course of study/examination during your notice period, you must either take annual leave or unpaid leave. Any time off must be agreed prior to taking the time.


4. In cases of non-completion the Directors will consider very carefully the reasons for non-completion. It is essential that individuals are not treated unfairly or penalised for circumstances beyond their control. Only where you have no valid reason for failing to complete would you be required to repay the costs.


**By signing your contract of employment you are agreeing to this training repayment policy.**


***Table 1***
| LENGTH OF SERVICE AT LEAVE DATE | AMOUNT RECOVERABLE (OF TOTAL COST) |
|----|----|
| Less than 12 Months | All |
| 12 Months, 1 Day – 13 Months | All |
| 13 Months, 1 Day – 14 Months | 11/12 |
| 14 Months, 1 Day – 15 Months | 10/12 |
| 15 Months, 1 Day – 16 Months | 9/12 |
| 16 Months, 1 Day – 17 Months | 8/12 |
| 17 Months, 1 Day – 18 Months | 7/12 |
| 18 Months, 1 Day – 19 Months | 6/12 |
| 19 Months, 1 Day – 20 Months | 5/12 |
| 20 Months, 1 Day – 21 Months | 4/12 |
| 21 Months, 1 Day – 22 Months | 3/12 |
| 22 Months, 1 Day – 23 Months | 2/12 |
| 23 Months, 1 Day – 24 Months | 1/12 |


\
