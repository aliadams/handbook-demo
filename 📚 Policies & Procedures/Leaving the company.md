# Leaving the company

We realise that from time-to-time people will leave the Company. It is important that your working relationship with us continues as normal during this period to ensure an effective handover of your role.

# **Notice**

Should you decide to leave the Company, you must advise your Manager of the fact in writing. You must serve the notice period as outlined within your Contract of Employment, unless agreed otherwise by mutual consent. Statutory notice period from the employer is one week if the employer has worked more than one month but less than two years; and then two weeks if worked more than two years; and one additional weeks’ notice for every completed years’ service up to a maximum of 12 weeks. These notice period requirements are in place so that the Company has sufficient time to find a replacement. Similarly, if the Company serves notice to terminate your contract, you will be required to work your notice period if the Company wishes you to do so.

# **Garden Leave**

Once either side has given notice of termination of employment, Telcom may, at any time and for any period, require you to cease performing your job and/or exclude you from entering any Telcom premises. You will also not be able to contact any of our customers or business contacts during this time. During such period of garden leave, Telcom will continue to pay your salary and provide all benefits that form part of your contract of employment.

# **Holiday leave**

If you have any annual leave or TOIL outstanding, this may either be taken during the period of notice (with the agreement of your Manager) or payment may be made in lieu of annual leave and/or TOIL in your final pay. A deduction will be made from your final pay if you have taken more holiday than you have accrued at this date.

# **Exit Interviews**

The Company aims to conduct exit interviews for employees who have resigned, to gain feedback. This may be carried out by a Line Manager or a designated representative.

# **Retirement**

Retirement would reflect that of the statutory regulations at all times. Please seek further advice from your Manager. You can obtain information regarding planning for retirement from https://www.gov.uk/plan-retirement-income.

# **Redundancy**

Whilst it is our aim to provide all employees with job security, circumstances do change. Where this means that there is a requirement for fewer employees, some redundancies may be necessary. In the event of a potential redundancy situation, the Company will follow legal obligations and its commitment to fair procedures which minimise job losses.

# **References**

We are committed to giving accurate and timely references for past employees and will supply a standard reference. References will only be provided to prospective future employers and not directly to employees. All references must be completed by the Manager and/or countersigned by the Director. In cases of redundancy the Company will,

where possible, agree with the redundant employee a suitable form of reference to be given to prospective new employers and to other interested parties including Jobcentre Plus.


Any employee is at liberty to provide a personal reference for an employee/ex-employee should they wish to do so. Any such reference, however, should make it explicit that it is a personal reference and not employer’s reference that is being supplied, and they must never be written on Company stationery.

# **Company Issued Property and Clothing**

If your employment is terminated for any reason, you will need to return all company property allocated to you during the course of your employment, whether this has been as a contractual benefit or to help you do your job. All property will need to be returned on or before your last day of work. Property should be returned to your manager and could include things like uniform, credit cards, security passes, keys, other equipment whether electronic or otherwise and any other property relating to the business or belonging to us, together with all copies of any such documents that you have in your possession or under your control. Failure to return our property will entitle us to withhold any final payments due to you and/or deduct the cost of replacement items.

# **Payment of final salary**

Any final payments of salary and expenses will be made to you in the normal way, less any deductions due to the company. Once all final payments have been made, you will be sent your final pay slip and relevant statutory forms such as your P45.