# Equality, Diversity & Inclusion Policy 

# 1 Introduction

1.1 Telcom is committed to promoting equality of opportunity, tackling discrimination
and valuing the diversity of both our staff, clients and the communities we serve.


1.2 The policy is based on the legal requirements to comply with the Equality Act 2010 and the anti-discrimination legislation protecting people from being discriminated against on the grounds of their:


* age
* disability
* gender reassignment
* marriage and civil partnership
* pregnancy and maternity
* race
* religion or belief
* sex
* sexual orientation

  \

1.3 Telcom wishes to create and sustain an organisational culture which values people from all sections of the community and the contribution each individual can make to Telcom work.


1.4 Telcom is committed to promoting equality throughout the organisation and

requires all staff, and partners to participate fully in achieving our aims.


1.5 Telcom will ensure appropriate training, guidance and advice are provided to meets these aims and commitment.

# 2 Policy

2.1 The policy provides a framework for monitoring performance and achieving best practice across the organisation in all business areas including employment, service provision and governance. 2.2 Telcom will ensure equality and welcome diversity in all aspects of employment policy and practice including:


* Recruitment and selection
* Training and development
* Consultation and participation
* Pay and benefits/ pensions and benefits
* Grievance and employee welfare
* Appraisal and promotion
* Membership of a recognised trade union
* Non-members of a trade union.

# **3 Procedure**

## 3.1 Our commitments

Through effective governance and management we are committed to:


* Promoting equality, diversity and social inclusion amongst our clients, staff, and partners and all those we work with;
* Challenging and eradicating discrimination wherever we encounter it on the grounds of age, disability, gender reassignment, marriage and civil partnership, pregnancy and maternity, race, religion or belief, sex, and sexual orientation;
* Treating everyone with dignity and respect at all times; • Providing responsive and accessible services that meet individual needs;
* Reviewing and auditing our equality, diversity and inclusion activities;
* Ensuring we keep our approach up to date with changes in society, legislation and regulation. We take seriously our legal duty to provide all our services and employment opportunities fairly and without discrimination, and we keep to all relevant codes of practice.

## **3.2 Meeting our commitments**

Our services We meet these commitments by:


* Promoting an environment that provides all our clients a service free of discrimination and prejudice;
* Removing any barriers that limit access to services and where possible tailor service delivery to meet individual needs;
* Providing accessible information and a variety of ways in which our customers can have their say on all our services;
* Building on good practice. Our staff We meet these commitments by working towards:
* Being an inclusive employer;
* Positively valuing the contribution of staff;
* Challenging and eliminating all discrimination;
* Developing and promoting recruitment policies and practices that give everyone equal access to employment opportunities;
* Exercising zero tolerance of bullying and harassment, and ensuring that everyone knows about their rights of protection;
* Making sure that all managers and staff know and understand the purpose of this policy.

# **4. Monitoring and Reviewing**

4.1 It is the responsibility of insert responsible person/s to ensure that our equality, diversity and inclusion aims are kept under review and are implemented throughout Telcom.

4.2 Telcom has specific accountability for monitoring the implementation of this
equality, diversity and inclusion policy.

4.3 The policy is reviewed every two years, and earlier in the event of significant
social, legal or regulatory changes.


\
