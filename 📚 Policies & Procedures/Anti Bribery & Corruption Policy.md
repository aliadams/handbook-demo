# Anti Bribery & Corruption Policy

Telcom Networks and Telcom Infrastructure is committed to carrying out business fairly, honestly and openly. The company operates a zero-tolerance approach to bribery and corruption and acts professionally, fairly and with integrity in all our business dealings and relationships. We will work hard to ensure full compliance with all applicable anti-bribery and corruption laws in the UK.


All employees and directors are prohibited from making any payment or inducement on behalf of Telcom Networks and Telcom Infrastructure to a third party which could be construed as a bribe. All payments to third parties including gifts or hospitality must comply with our policies and procedures.


Danny Smith has board level responsibility for anti-bribery and corruption within Telcom Networks and Telcom Infrastructure.

# **What is a Bribe?**

The giving and receiving of money, a gift or other advantage as an inducement to do something that is dishonest, illegal or a breach of trust in the course of business.

## **UK Bribery Legislation**

The UK Bribery Act 2010 came into eﬀect on 1 July 2011. It has created four new criminal oﬀences:

1. Giving, promising or oﬀering a bribe

1. Requesting, agreeing to receive or accepting a bribe

3. Bribing a foreign public oﬃcial

4. Failure by a commercial organisation to prevent active bribery being committed on its be**half.**

## **Who does the Act apply to?**

It applies to all commercial organisations, whether in the public or private sector, regardless of size with operations in the UK. This includes overseas companies with a presence in the UK.

## **What are the consequences of getting it wrong?**

**I**ndividuals can be sent to prison for up to 10 years, and individuals and businesses can be subject to unlimited ﬁnes.



---

# Policy

This policy sets out our responsibilities in upholding our position on bribery and corruption and provides information and guidance on how to recognise and deal with bribery and corruption issues and the procedures in place in respect of the following areas:


* Corporate Hospitality
* Gifts
* Expenses
* Charitable Giving
* Political Donations
* Sponsorships


## Corporate Hospitality

This policy does  not  prohibit normal and appropriate hospitality (given and received) to or  from third parties.


Should any member of staﬀ receive an invitation to attend an event or occasion through their position/job with the company, let the company know about this (in case it is part of a wider pattern of behaviour) using the [Reporting Escalation Policy](/doc/reporting-escalation-policy-NyzFmpkoUp).


A completed Gift/Hospitality/Sponsorship form should be completed and authorised by the person identified in the [Reporting Escalation Policy](/doc/reporting-escalation-policy-NyzFmpkoUp) before the invitation can be accepted.

## Gifts

Should any member of staﬀ receive a gift from  a third party for example a company with  whom you have dealings or a client, a Gift/Hospitality/Sponsorship Form must be completed, signed oﬀ by a senior manager and placed in the Gift Register. This is held by [HIDDEN]

## Expenses

Reasonable expenses  incurred  as part of carrying out duties may be submitted using the [Expenses Policy](/doc/expenses-policy-how-to-claim-Y1j8HTDOJS).

## Charitable Giving

Should any member of staﬀ wish to donate to any charitable event/organisation on behalf of Telcom Networks and Telcom Infrastructure they must complete a Gift/Hospitality/Sponsorship Form and submit to the person identified by the [Reporting Escalation Policy](/doc/reporting-escalation-policy-NyzFmpkoUp) for agreement.

## Political Donations

It is prohibited to make any political donation.

## Sponsorship

Should any member of staﬀ wish to sponsor a third party on behalf of the company a Gift/Hospitality/Sponsorship Form must be completed and returned to [HIDDEN]  This will be held in the Sponsorship Register.

# Review and Monitoring

All registers referred to in the above policy will be reviewed at regular intervals.

# Whistle-blowing Procedure

Should any person be concerned that improper activity is taking place they can speak in conﬁdence to Alistair Adams ([HIDDEN]) or a report identified by the [Reporting Escalation Policy](/doc/reporting-escalation-policy-NyzFmpkoUp).

# Due Diligence

Appropriate due diligence must be carried out for all

* Third Parties Relationships
* Staﬀ Recruitment


This will be done through checks on the following:

* References
* Any other checks deemed necessary

# Your Responsibilities

You must ensure that you read, understand and comply with this policy. You must notify your manager as soon as possible if you believe or suspect that a breach of the policy has occurred or may occur.


Any employee who breaches this policy will face disciplinary action, which could result in dismissal for gross misconduct.

# Record Keeping

We keep ﬁnancial records and have appropriate monitoring and internal controls in place which evidence the business reason for making payments to third parties. The forms completed are an integral part of this.

# Training and Communication

Training on this policy forms part of the induction process for all new employees.

# What to do if you are concerned a payment may be considered a bribe

If you are unsure if a payment could be considered a bribe, please refer it immediately to the person identified by our [Reporting Escalation Policy](/doc/reporting-escalation-policy-NyzFmpkoUp).