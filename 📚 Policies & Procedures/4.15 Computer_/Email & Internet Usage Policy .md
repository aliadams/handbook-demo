# 4.15 Computer / Email & Internet Usage Policy 

# Introduction

The Email/Internet Usage Policy applies to all employees of Telcom and any others who are offered access to the Company’s electronic resources. The Policy also covers any media files/ images / messages that can be transmitted by mobile phones.

# Policy

Use of the Internet and email systems by Telcom employees is strictly prohibited for personal reasons unless permission has been granted by their Line Manager.

Employees can be held accountable for any breaches of security or confidentiality and web filtering and monitoring for security and/or network management reasons will be undertaken regularly. There should be no expectation of privacy in any employee’s use of these systems and the monitoring or accessing of employees’ accounts will be compliant with the Regulation of Investigatory Powers Act.

## **Employees must:**

* Be responsible for the security and confidentiality of their ‘Username’ and Password’ and safeguard them from unauthorised use. ‘Usernames’ and ‘Passwords’ must be memorised or stored in a secure location;
* Remember that the Company values apply when using Telcom systems. Confidential, misleading or inaccurate information must not be disclosed;
* Where appropriate exercise caution when opening unsolicited or suspicious e- mails and where necessary ensure they are virus checked beforehand, and at any other time at management’s request return all Company information and data held in computer useable format;
* Upon leaving the Company’s employment all data belonging to the company should be disposed of as directed by the Directors and copies must not be made or kept.

## **Employees must not:**

* Attempt to visit Internet sites that contain obscene, pornographic, hateful or other objectionable materials. In the event of an incidental connection to a site that contains this kind of material, all employees must disconnect from it immediately. In addition, sexually explicit material must not be archived, stored, distributed, edited or recorded using the Telcom network or other computing resources;
* Use another employee’s password to log on to the computer system with or without that employee’s permission;
* Load or run unauthorised games or software or open documents from unknown origins;
* Use Telcom Internet facilities to propagate any virus type software or any other program code which has a mischievous or malicious intent;
* Use Telcom Internet facilities for personal commercial activity; 77 Telcom Company Handbook
* Circulate spam or chain emails. Such messages must be deleted immediately, without opening the message;
* Use Telcom Internet facilities for personal social media activity, i.e. Facebook, Twitter etc.;
* Use Telcom Internet facilities for personal instant messenger services such as Skype;
* Circumvent or disable any Telcom security features;
* Use Telcom systems to write a personal blog (even if done during an employee’s own time);
* Make disparaging remarks about Telcom, its clients or colleagues, at any time, on a personal blog or any form of social media.

## **Social networking sites**

* It is the responsibility of the individual to ensure that any social networking sites which are accessed are secure;
* It is the responsibility of the individual to ensure that any activity undertaken on such sites does not bring the company into disrepute.


All usage is governed by this policy. Any breaches of this policy may be regarded as an act of gross misconduct which may result in an employee’s dismissal.


