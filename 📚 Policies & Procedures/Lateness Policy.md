# Lateness Policy

Employees are expected to be on Telcom premises on time. Employees must be ready to work at the start of their contracted hours. Lateness will be recorded and monitored by Telcom. High Levels of lateness may lead to disciplinary action.