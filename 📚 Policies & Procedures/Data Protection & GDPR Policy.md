# Data Protection & GDPR Policy

# 1. Policy Statement

1.1 Every day our business will receive, use and store personal information about our clients, associates and colleagues. It is important that this information is handled lawfully and appropriately in line with the requirements of the Data Protection Act 2018 and the General Data Protection Regulation (collectively referred to as the ‘Data Protection Requirements’).


1.2 We take our data protection duties seriously, because we respect the trust that is being placed in us to use personal information appropriately and responsibly.

# 2. About This Policy

2.1 This policy, and any other documents referred to in it, sets out the basis on which we will process any personal data we collect or process.


2.2 This policy does not form part of any employee’s contract of employment and may be amended at any time.


2.3 Your line manager can direct you to the person responsible for ensuring compliance with the Data Protection Requirements and with this policy. Any questions about the operation of this policy or any concerns that the policy has not been followed should be referred in the first instance to the Data Protection Officer or reported in line with the organisation’s Whistleblowing Policy or Grievance Policy.

# 3. What is Personal Data?

3.1 Personal data means data (whether stored electronically or paper based) relating to a living individual who can be identified directly or indirectly from that data (or from that data and other information in our possession).


3.2 Processing is any activity that involves use of personal data. It includes obtaining, recording or holding the data, organising, amending, retrieving, using, disclosing, erasing or destroying it. Processing also includes transferring personal data to third parties.


3.3 Sensitive personal data includes personal data about a person’s racial or ethnic origin, political opinions, religious or philosophical beliefs, trade union membership, genetic, biometric, physical or mental health condition, sexual orientation or sexual life. It can also include data about criminal offences or convictions. Sensitive personal data can only be processed under strict conditions, including with the consent of the individual.


# 4. Data Protection Principles

4.1 Anyone processing personal data must ensure that data is:


A. Processed fairly, lawfully and in a transparent manner.

B. Collected for specified, explicit and legitimate purposes and any further

processing is completed for a compatible purpose.

C. Adequate, relevant and limited to what is necessary for the intended

purposes.

D. Accurate, and where necessary, kept up to date.

E. Kept in a form which permits identification for no longer than necessary for

the intended purposes.

 \n 