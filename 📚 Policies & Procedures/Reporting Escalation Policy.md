# Reporting Escalation Policy

By default, if you ever have a problem that you don’t feel able to manage yourself, you should raise it with a line manager, but sometimes (for example cases where the line manager is involved) that might not be appropriate.


Below is our recommended escalation policy in order of escalation. Keep going down the list until you find someone not connected with the issue you are raising:


1. Your direct line manager (who you do one-to-ones with)
2. The head of your department
3. The Telcom HR Team at [HIDDEN] (if you think HR can help with the issue)
4. The operational lead of your company. These are:
5. One of the founders
6. The group chairman


