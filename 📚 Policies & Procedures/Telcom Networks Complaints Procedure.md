# Telcom Networks Complaints Procedure


:::info
While this document should be considered our most up-to-date complaints procedure, a customer-facing version of this content is also maintained for sending to customers.  

[📁 View customer-facing version](https://docs.google.com/document/d/1VIUna7wqkAM6hwvQQEoQDyHWXXP5YD75vqnLgE2R2wM/edit).

:::


:::warning
If you update this policy, make sure to update the customer document as well

:::


At Telcom, we aim to give you the best customer service possible. But if you feel we’ve fallen short of this, please let us know. We’ll work with you to put things right and we’ll always try to use your feedback to improve things where we can.


This complaints process lets you know how to go about making a complaint and how to take it further, if you need to.

# How we try to resolve disputes
| 1 | **2** | **3** | **4** |
|----|----|----|----|
| Once you’ve complained we’ll try to fix the issue straight away. If we can’t do that we’ll tell you how long it’s going to take or when we can get back to you with an update. | If you’re not happy with what we’ve done you can ask us to escalate your complaint anytime. But please let us try to sort things out first | If you’re not happy you can escalate again and we’ll complete a final complaint review | We’ll only close your complaint when you say it’s fixed or if we haven’t heard from you in 28 days (we’ll try to contact you before we do that). |


## 1. Let us know the issue

What to do if you’re not satisfied with our service:

 \n If you’re unhappy with our service, please let us know as soon as you can. We’ll do our best to put things right, so you can carry on enjoying your services again. We’ll also use your feedback to improve our service where we can.


See how to contact us on help.telcom.uk/contact

 \n If you would like to continue with a complaint, we’ve set out our formal internal complaints procedure below. We are fully committed to addressing all complaints, fully and fairly, and in a reasonable timeframe.

 \n We prefer to resolve complaints by telephone – but if you’d prefer to receive a response in writing, then please ask.

## 2. Making a complaint

There are 3 easy ways to get in touch. So we can get back to you sooner, let us know details like your account number, address and contact number when you contact us.

 \n **By phone**

Call us on 0330 122 2970 and our Customer Care team will be able to assist you. We will try our best to resolve your complaint during your initial phone call. However, where this is not possible, we will agree a course of action with you and provide you, where possible, with clear timeframes and next steps for the resolution of your complaint.


**By email**

Send an email to [HIDDEN] to contact our complaints team. They’ll work hard to resolve your problem, but just to make sure the process isn’t slowed down, we ask you not to send correspondence to individual team member’s email addresses. Your email will be acknowledged within 2 working days of receipt and we aim to resolve your complaint within 28 days of receipt.

 \n **By post**

It’s not as quick, but if you’d prefer to send us a letter, you can write to us at the following address:


> Complaints department
>
> Telcom Networks Ltd
>
> 5th Floor
>
> Hilton House
>
> 26/28 Hilton Street
>
> Manchester
>
> M1 2EH

 \n Your letter will normally be acknowledged within 2 working days of receipt and we aim to resolve all written complaints within 28 days of receipt.

## 3. Escalation

When we receive your complaint, we’ll aim to resolve it to your complete satisfaction. If a complaint is not resolved to your satisfaction you can ask to escalate the issue to a manager. A manager may be available to speak to you immediately or they may call you back at a time that suits both of you. In the event that a manager has to call you back, you can help us by letting us know which daytime and evening contact numbers work best for you.


If, after following the process explained above, you are still not happy with the outcome of your complaint, you can refer your complaint to independent adjudication.

## 4. Independent Adjudication

If we have not reached an agreed settlement within eight weeks of receiving your complaint, or we agree in writing before the eight weeks is up that the dispute should be settled by independent adjudication, you can refer your complaint for independent consideration through Alternative Dispute Resolution.


This service is absolutely free of charge and accessible at ombudsman-services.org.


**___**

 \n *DX92776888*