# Holidays & Leave

The following sections summarise some of the obligations we expect you to comply with when working for us. Further information relating to some of these sections can be found in our policies and will be indicated below.

# **Holidays and Leave Entitlement**

The Holiday year runs from 1 December to the 30 November of each year. Your Holiday Entitlement can be found within your Contract of Employment. Employees joining Telcom part way through a holiday year will receive the appropriate proportion of that year’s entitlement based on the completed calendar months of service.


Holidays must be taken at times convenient to the Employer and sufficient notice of intention to take holidays must be given to your Manager. No more than 10 days’ holiday may be taken at any one time unless permission is given by your Manager.

# **Booking Procedure**


:::info
To book time off, go to the [how to book time off](/doc/if-you-want-to-book-time-off-a61KCa1piC) article.

:::

# **Special Leave**

Special leave is not to be regarded as an automatic entitlement with which to supplement annual leave or emergency leave.


Times of close family bereavement and critical illness are particularly stressful and the company recognises the need to support staff in such circumstances in a number of ways. The granting of special leave with pay to facilitate time off from work is one such measure. For the purposes of special leave, a 'close family' means a member of an employee’s immediate family (mother, father, partner, sister, brother, daughter, son).


Whenever there is a bereavement of a close relative, as defined above, in order to enable the making of funeral arrangements, the handling of personal matters and attendance at the funeral, special leave may be granted. In certain limited circumstances, special leave may also be granted in the circumstances of a close relative being taken critically ill.


The amount of special leave granted, will be up to a maximum of 5 days. This will depend on the particular circumstances of the employee. Leave in respect of relatives other than close relatives is to be restricted to 1 day without pay.

# **Other entitlements:**

## **Time off for dependants**

An employee may be entitled to take a reasonable amount of unpaid time off during working hours to take action that is necessary to provide help to your dependants. A dependant could be a spouse, partner, child, grandchild, parent, or someone who depends on you for care. This time off is intended to deal with unforeseen matters and emergencies only and not a situation you had prior knowledge of, for example you would not be covered if you wanted to take your child to hospital for a prearranged appointment. Should you require time off for dependants you should inform your line manager at the earliest opportunity. Please note requesting time off by text/email is not permitted. The Company will monitor any time off and should it have any concerns these will be discussed with you. If you are found to be abusing this right, disciplinary action may be taken.

## **Medical/Dental Appointments**

Medical and dental appointments should be arranged in an employee's own time unless there is an emergency requirement to attend during working hours. In the event that non-emergency appointments with a GP or dentist should occur during the working day or shift, this should be at the beginning or end of the shift.


Where an employee is able to show that he or she is unable to get an appointment outside working hours or at the start or end of a shift, it is at the discretion of the manager to allow reasonable paid time off for the appointment.

## **Ante-Natal Clinics**

Female employees are allowed time off with pay to attend ante-natal clinics upon presentation of their appointment card and only for the time to travel to / from (if in normal work time) and the appointment period. Partners of a pregnant lady are allowed time off unpaid to attend 2 antenatal appointments.

## **Magisterial Duties**

Employees who are magistrates are allowed reasonable time off without pay to undertake their magisterial duties. Employees who are granted such time off for these purposes shall receive the allowance payable by the Magistrates Court Committee.

## **Jury Service**

Reasonable time off, without pay, is granted to employees for this purpose. Employees must claim loss of earnings’ allowance from the Courts.

## **Court Witness**

Only employees who are witnesses for the Company will be allowed time off with pay. Time off, without pay, may be granted for any other type of witness duty.

## **Members of a Local Authority**

Employees who are elected to serve as Members (i.e. Councillors) with a Local Authority shall be granted reasonable time off, without pay, to allow them to perform their duties in that capacity and that they be allowed to retain in full, any attendance allowances paid to them as a result.

## **Parental Leave**

Parental leave is a right that allows parents to be absent from work for the purpose of caring for a child. Parents can use it to spend more time with children and strike a better balance between their work and family commitments. This right came into force on 15th December 1999.

### **Key Features**

* The rights apply for children born after 15th December 1999, and children under 18 adopted after that date.
* The rights apply to natural and adoptive parents.
* Employees must have one year’s service to qualify.
* The entitlement is up to 18 weeks leave per parent per child up to their 18th birthday.
* The Employee will remain employed during parental leave but it will be unpaid and the only terms and conditions that will continue are those relating to good faith and confidentiality.
* At the end of the leave the Employee is entitled to his or her own job back (or one of the same or better status if it is not possible to be employed in a previous job)
* Part-timers will receive pro-rated parental leave.
* Employees taking parental leave are protected from detriment and unfair dismissal

### **How the Scheme Works**

* In blocks of one week at a time up to a maximum of 4 in any year (per child).
* In blocks of one day or multiples of one day if leave is to care for a disabled child (up to a maximum 4 weeks in a year).
* The employee must give at least 21 days’ notice of start and finish dates (not necessarily in writing).

  \

The Company has the right to postpone leave for up to 6 months for business reasons unless the leave is to take place immediately after birth or adoption

## **Hours of Work**

Hours of work are detailed within your Contract of Employment. A flexible approach to working hours may be necessary due to the nature of our business. When known in advance, this will be contained in your Contract of Employment. In the event of a change being required, this will be discussed with you in advance by your Manager.


It is inevitable, due to the industry and market place competition that your hours of work will be subject to change at any given time. Should a permanent change be required, this will be addressed via consultation process.

# **Overtime Working**

You may have the opportunity to undertake overtime with Telcom from time to time on a voluntary basis in order to meet the needs of the business. All overtime must always be approved prior to commencement. You will receive time back in lieu for any agreed overtime during the week. Any overtime undertaken during the weekend will be paid at your normal hourly rate.

# Working Time Regulations

By signing your contract of employment you are agreeing to opt out of the Working Time Directive, which will provide both you and the company with more flexibility during the working week.


The Opt-Out Agreement is made under the provisions of the Working Time Directive (WTD) 1998. Should you wish to sign yourself back into the Working Time Directive, please seek further advice from a Director

# **Flexible Working**

The company will consider all relevant applications of flexible working requests. Please see Flexible Working policy for more details.

# **Lay Off Periods / Short-time Working**

There may be times in the year when workload becomes minimal or diminished entirely. In order to manage these periods (however long or short), Telcom may, in the first instance either lay-off employees or offer a short-time working arrangement. In the event that you are laid off due to a temporary cessation of work, your entitlement to pay during that period of layoff will cease save that your rights to a statutory guarantee payment pursuant to your statutory rights are not affected.

# **Sickness absence**

More detail around taking absence in cases of being unwell can be found in our [Sickness Policy](/doc/46-sickness-absence-policy-and-lateness-policy-6ellOW1NbN).

# **Absence without authorised leave**

If you do not follow the absence reporting procedure, your absence will be treated as unauthorised and we reserve the right to stop your pay and action may be taken in line with the Disciplinary Policy and Guidelines.


