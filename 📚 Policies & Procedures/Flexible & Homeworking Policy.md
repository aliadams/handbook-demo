# Flexible & Homeworking Policy

**Telcom is committed to providing equality of opportunity in employment and to developing work practices and policies that support work-life balance.** We recognise that, in addition to helping balance work and personal lives, flexible working can raise employee morale, reduce absenteeism and improve our use and retention of employees.


# **What is the Right to Request to Work Flexibly?**

As of 30 June 2014 the right to request flexible working applies to all employees, providing you have 26 weeks continuous service (not just those with parental responsibility for a child, or caring responsibilities for an adult).


It is important to understand that this is not a right to work flexibly but to ask Telcom to seriously consider whether such a way of working is an appropriate alternative to the way in which you currently work.


# **Eligibility**

You must meet the following criteria to be able to make a request to work flexibly:

1. Have 26 weeks' continuous service with Telcom;
2. Have not made another application within the previous 12 months.
3. Making an Application an application to work flexibly must be in writing and specify the following:
4. That the request is made under the terms of the flexible working legislation;
5. How you meet the eligibility criteria of service;
6. The requested change and the date you would like this to be effective;
7. Any impact this change may have on Telcom and suggestions of how this may be overcome;
8. Whether any previous request to work flexibly has been made and, if so, when.


The terms and conditions you can request to change only relate to hours, start/finish times and place of work.An application must be made in writing; however it can be submitted via letter or email. See Appendix E in the original Handbook document on Google Drive for sample.


# **Consideration of the Request**

Telcom will give serious and full consideration to all requests to work flexibly from eligible employees. If a request cannot be agreed without further discussion a meeting will be arranged within 3 months to discuss this and consider alternative flexible options.


Although there is no legal right to be accompanied to the meeting, the Company will allow your chosen colleague to accompany you for support with their agreement. If you or your work colleague are not available for the meeting then an alternative time suitable for all parties should be agreed and held within seven days.


If Telcom cannot meet your request it will be on one or more of the following grounds:

* The burden of additional costs
* An inability to reorganise work amongst existing staff
* An inability to recruit additional staff
* A detrimental impact on quality
* A detrimental impact on performance
* Detrimental effect on ability to meet customer demand
* Insufficient work for the periods the employee proposes to work
* A planned structural change to your business

  \

Whatever the outcome of the meeting the result will be confirmed to the employee in writing within 14 days of the meeting, unless an extension is agreed.


If the requested change of working arrangements is accepted then the new terms and conditions that apply are permanent. You have no right to return to your old terms and Telcom has no right to enforce a return unless a trial of the new terms has not proved successful.


# **Trial Period**

Telcom may decide to offer the new way of working on a trial basis initially if it is not sure what the impact of it will be on the business. In such circumstances, the change to your terms and conditions of employment during the trial period is a temporary change to your terms and conditions of employment.

If the trial is not successful, you will revert back to your previous terms and conditions of employment. If the requested change of working arrangements is accepted at the end of the trial then the temporary terms and conditions will become permanent.

# **The Appeals Process**

You have the right of appeal if your application to work flexibly is declined and must put your appeal in writing within 14 days of receipt of Telcom decision, unless an extension is agreed.

The letter of appeal must clearly state the grounds for appeal and a further meeting will then be arranged in the same manner as the original hearing giving you the right to be accompanied.

Following the appeal hearing Telcom will confirm the outcome in writing within a further 14 days (unless an extension has been agreed) detailing any agreement that has been met or an explanation of the grounds for dismissing the appeal.

The decision at the appeal hearing stage is final.


# **Withdrawing an Application**

A request to work flexibly can be withdrawn at any time before it has been accepted and any new terms and conditions agreed and put in place. If you withdraw your application you will not be eligible to make another flexible working request for a further 12 months.

If you fail to attend more than one meeting arranged to discuss the request and do not provide a reasonable explanation,Telcom may assume that the application has been withdrawn. This will be confirmed to you in writing.

Telcom may also treat an application as withdrawn if the employee does not provide the required information.