# Expenses Policy + How to claim


:::tip
Want to know how to claim an expense? Read to the ‘[How to claim](#h-how-to-claim)’ section after the Expanses Policy

:::

# Expenses Policy

Telcom will reimburse employees for reasonable expenses wholly, necessarily and exclusively incurred by employees in connection with its business. Employees should not be either financially disadvantaged or advantaged because of genuine business expenses.

Breach of this policy is a disciplinary offense. Deliberate falsification of a claim or the evidence needed to make a claim will constitute gross misconduct. This may result in summary dismissal. "Falsification" includes the failure to pass on any discount obtained in the course of incurring an expense.

# Allowable expenses

## 🚄 UK travel

We will reimburse the reasonable costs of amounts necessarily expended on travelling in the performance of the duties of the employment, for example to events, external meetings, training etc.

This does not include travel between home and office unless the employee is recognised as being a "home-based employee".

Employees should travel by the most cost-effective mode of transport taking into account journey time and the nature of the journey as well as the monetary cost.

### By employee's own car/motorcycle/bike

The mileage allowance is set by reference to the HM Revenue and Customs (HMRC) rules on AMAPs (Approved Mileage Allowance Payments). These are currently as follows:
| Type of vehicle | First 10,000 miles | Over 10,000 miles |
|----|----|----|
| cars and vans | 45p | 25p |
| motorcycles | 24p | 24p |
| cycles | 20p | 20p |

The employee is required to keep track of business mileage incurred in the tax year (6 April to 5 April) to ensure the correct rates are used.

If an employee carries any other employees in their own car or van on business travel, that employee can claim 5p per passenger per business mile. The passengers must be employees and they must also be travelling on business journeys.

When an employee uses their own vehicle, the employee should ensure that the vehicle is in good working order, fully insured, taxed and MOT’d and that the driver is covered by breakdown assistance.

The department head may request the employee to print off a route planner to support the business mileage claimed by that employee.

### By taxi

Employees are expected to use taxis only when strictly necessary and where it is cost effective to do so. Employees should always obtain a receipt.

### By tube, bus or tram

Employees should retain tickets where possible. Where an employee's season ticket already covers the cost of travel, no further claim will be allowed. Where Oyster cards are used, the employee should provide evidence of the additional cost incurred, for example by printing off their online statement or by printing off the journey cost provided by the Oyster fare finder.

### By train

Employees should, as far as possible, travel standard class and should book their journeys sufficiently in advance to obtain the best possible prices.

First class train travel is acceptable if the journey from London mainline stations (not the employee's home address) is timetabled to last in excess of two hours and the employee needs to work, or at the manager's discretion, eg for very early starts, late returns, where standard class would be exceptionally crowded. Employees should retain all tickets and credit card vouchers.

### By plane

Flights should, whenever possible, be booked sufficiently in advance to obtain the best possible prices. Air travel should be by economy class. Employees should retain all tickets and credit card vouchers.

### Late night travel

In exceptional circumstances, and only in accordance with HMRC rules, Telcom may meet the cost of late night taxis or similar road transport provided all the following circumstances apply:

* the employee is required to work later than usual and at least until 9.00pm;
* this occurs irregularly; and
* by the time the employee ceases work either public transport has ceased or it would not be reasonable to expect the employee to use public transport.

The number of such journeys must not exceed 60 in any tax year.

## 🛌 UK overnight accommodation

### Hotel accommodation

Hotel bookings should be requested in advance via [HOTEL REQUEST FORM].

Employees should take into account the location of the hotel as regards the cost of taxis, etc and the time required to travel to and from the hotel.

In rare cases where arranging a hotel in advance is not possible, for example an unplanned stay due to a vehicle breakdown, employees should endeavour to choose the most cost effective hotel and room. Rates should not exceed £130 per night (£150 within the M25), including VAT but excluding breakfast costs. Any booking in excess of that amount will need to be approved by the department head prior to booking.

### Staying with friends or relatives

An employee may claim an allowance of up to £35 per night. Receipts are not required but the employee must confirm that the overnight stay was necessary in connection with travelling on Telcom business.

An employee claiming this allowance cannot also claim reimbursement of actual accommodation and subsistence costs although they can claim personal incidental expenses (see below).

In all cases employees should retain all invoices.

## 🥙 UK subsistence

### When staying overnight

If you are required to be away from your normal base overnight on business, you can claim up to (inclusive of tax and other charges):

* £6 for a breakfast following an overnight stay
* £6 for lunch
* £16 for dinner on the night of the stay
* parking at hotel

Additionally, the employee may claim "personal incidental expenses" up to a maximum of £5 per day. This covers items such as telephone calls, newspapers and laundry. The total of any such costs must not exceed £5/day otherwise no reimbursement will be allowed, ie the allowance should not be regarded as a contribution to such costs.

If the employee is away for more than one night, the daily limits may be averaged across the number of nights that the employee is away.

In all cases employees should retain all invoices.

### When not staying over night

You can claim up to a total (inclusive of tax and other charges) of £6 towards a meal in situations where you are acting on Telcom business and have had to work away from your typical place of work for over 5 hours, inclusive of travel time and it is not practical for you to return home or to the office during that time. If these criteria are met during a period which falls out of 8pm, you can increase that limit to £16.

Personal incidental expenses cannot be claimed where there is no overnight stay.

In all cases employees should retain all invoices.

## Overseas trips

The rules above apply to overseas travel as well as UK travel. However, in some cases, it may be necessary to allow higher limits for accommodation and subsistence costs, subject to approval from the Department Head. The intention is that the employee should be able to obtain accommodation and meals of an equivalent standard to that available to them in the UK. Please note that there is no scope for higher rates for mileage.

Air travel should normally be by economy class, or if outside Europe, by business class.

The maximum allowed for personal incidental expenses for overseas trips is £10 per day. This covers items such as telephone calls, newspapers and laundry. The total of any such costs must not exceed £10 otherwise no reimbursement will be allowed, ie the allowance should not be regarded as a contribution to such costs.

Where expenses are incurred in the local currency, the claim for reimbursement should either be for the actual sterling amount debited to the employee's debit/credit card or, where paid in cash, should be converted at the rate applicable on that day.

In all cases employees should retain all invoices.

## 👔 Entertaining

### Business

Employees should only entertain visitors and guests where it is likely to assist Telcom in its corporate objectives. Wherever possible, the café facilities should be used.

In general, entertainment counts as business-related if its purpose is to discuss a particular Telcom project, maintain an existing Telcom connection or to form a new Telcom connection.

By contrast, entertainment of Telcom acquaintances won't count as business-related if its purpose is really social - even if there's some discussion of business-related topics in the course of the entertainment.

Amounts claimed should be reasonable and appropriate.

In all cases employees should retain all invoices. Please show names and organisation of all attendees on the expense claim.

### Staff

The cost of entertaining other Telcom staff is not reimbursable. Managers are able to use their department's reward and recognition budget for any staff entertaining, etc.

## 👓 Eye tests and spectacles

Telcom recognises its obligations under the Display Screen Equipment (DSE) Regulations 1992 (Amended 2002). The Ombudsman Service will contribute to the cost of an eye test if you use display screen (computer monitor) equipment for a significant part of your working day.

Telcom will contribute £25 towards the cost of an eye test conducted by a suitably qualified optician. If the test reveals that spectacles are required for exclusively VDU work, Telcom will contribute £75 towards the cost of basic spectacles. This policy does not apply to contact lenses.

Telcom will only reimburse one eye test in any 12-month period.

Telcom will not contribute towards the cost of spectacles with any element of everyday use, in other words the spectacles must be exclusively for VDU use.

# How to claim